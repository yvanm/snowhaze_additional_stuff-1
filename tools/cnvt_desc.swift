import Foundation
import AppKit

extension String {
	var nsstring: NSString {
		return self as NSString
	}
	
	func replaced(pattern: String, with: String) -> String {
		let rx = try! NSRegularExpression(pattern: pattern, options: NSRegularExpressionOptions())
		let rng = NSMakeRange(0, nsstring.length)
		return rx.stringByReplacingMatchesInString(self, options: NSMatchingOptions(), range: rng, withTemplate: with)
	}
}

let pb = NSPasteboard.generalPasteboard()
let content = pb.stringForType(NSPasteboardTypeString)
guard let a = content else {
	print("could not read clipboard")
	exit(0)
}

let trimmed = a.replaced("\\s*<(/)?p>\\s*", with: "<$1p>").replaced("\\s+", with: " ")
let result = trimmed.replaced("<(/)?p>$", with: "").replaced("<p>(?!.*</p>)", with: "")
pb.declareTypes([NSPasteboardTypeString], owner: nil)
if pb.setString(result, forType: NSPasteboardTypeString) {
	print(result)
} else {
	print("failed to modify clipboard")
}