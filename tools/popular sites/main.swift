// compile flags: -import-objc-header ../../../https-check/bridge.h -O ../../../https-check/SQLCipherDatabase.swift ../../../https-check/sqlite3.o

import Foundation

let desktopPath = NSSearchPathForDirectoriesInDomains(.DesktopDirectory, .UserDomainMask, true).first! as NSString
let db = SQLCipherDatabase(path: desktopPath.stringByAppendingPathComponent("https.db"), key: "")!

func check(s: String) -> String? {
	let query = "SELECT domain FROM sites WHERE (:domain = domain OR ('www.' || :domain) = domain) AND http >= 10 AND http >= 0.97 * https"
	let checkedDomains = try! db.execute(query: query, withBindings: [.Alphanumeric(":domain"): .Text(s)])
	switch checkedDomains.count {
		case 0:		return nil
		case 1:		return checkedDomains.first!["domain"]!.text
		case 2:		return s
		default:	fatalError("Domains should be unique")
	}
}

let csv = try! String(contentsOfFile: "/Users/andris/Repos/Bitbucket/Illotros/https-check/top-1m.csv", usedEncoding: nil)
let lines = csv.componentsSeparatedByString("\n").filter {!$0.isEmpty}
let domains = lines.map {check($0.componentsSeparatedByString(",")[1])}
let correct = domains.filter {$0 != nil}
let final = correct.map {$0!}
for domain in final {
	print(domain)
}