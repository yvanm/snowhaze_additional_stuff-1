//compiler flags: -o check -import-objc-header ../../../https-check/bridge.h -O ../../../https-check/sqlite3.o ../../../https-check/SQLCipherDatabase.swift

let parallelism = 100

import Foundation

let data = try! String(contentsOfFile: "private.domains")
let lines = data.componentsSeparatedByString("\n")
let domains = lines.filter {!$0.isEmpty}

var cont = true

var outstanding = 0

var urls = [(NSURL, String, Bool)]()
let session = NSURLSession(configuration: NSURLSessionConfiguration.ephemeralSessionConfiguration())
let desktopPath = NSSearchPathForDirectoriesInDomains(.DesktopDirectory, .UserDomainMask, true).first! as NSString
let db = SQLCipherDatabase(path: desktopPath.stringByAppendingPathComponent("private.db"), key: "")!
let queue = dispatch_queue_create("ch.illotros.httpscheck.db-queue".cStringUsingEncoding(NSUTF8StringEncoding), DISPATCH_QUEUE_SERIAL)

func setup() {
	if !db.has(table: "sites") {
		try! db.execute(query: "CREATE TABLE sites (domain TEXT UNIQUE ON CONFLICT IGNORE, direct INTEGER NOT NULL, www INTEGER NOT NULL)")
	}
	for domain in domains {
		urls.append((NSURL(string: "https://" + domain)!, domain, false))
		urls.append((NSURL(string: "http://" + domain)!, domain, false))
		urls.append((NSURL(string: "https://www." + domain)!, domain, true))
		urls.append((NSURL(string: "http://www." + domain)!, domain, true))
		outstanding = urls.count
	}
}

func start() {
	dispatch_async(queue) {
		for _ in 0 ..< parallelism {
			tryNext()
		}
	}
}

func tryNext() {
	guard !urls.isEmpty else {
		return
	}
	let (url, domain, isWWW) = urls.removeFirst()
	let task = session.dataTaskWithRequest(NSURLRequest(URL: url)) { (data, response, error) in
		dispatch_async(queue) {
			let sqldomain = SQLCipherDataTypes.Text(domain)
			if error == nil && data != nil && response?.URL?.host == url.host {
				try! db.execute(query: "INSERT INTO sites VALUES (?, 0, 0)", withBindings: [sqldomain])
				if isWWW {
					try! db.execute(query: "UPDATE sites SET www = www + 1 WHERE domain = ?", withBindings: [sqldomain])
				} else {
					try! db.execute(query: "UPDATE sites SET direct = direct + 1 WHERE domain = ?", withBindings: [sqldomain])
				}
			}
			tryNext()
			outstanding -= 1
		}
	}
	task.resume()
}

setup()
start()
var oldPercent = 0
while outstanding > 0 {
	sleep(1)
	let percent = 100 - (100 * outstanding + 4 * domains.count - 1) / (4 * domains.count)
	if percent > oldPercent {
		oldPercent = percent
		print("\(percent) %")
	}
}
print("done")