import Foundation

let tmp = try! String(contentsOfFile: "/Users/andris/Desktop/tmp_private.domains")
let tmpDomains = tmp.componentsSeparatedByString("\n")
assert(tmpDomains.filter({$0.containsString("/")}).isEmpty)
var counts = [String: Int]()
for domain in tmpDomains {
	if let count = counts[domain] {
		counts[domain] = count + 1
	} else {
		counts[domain] = 1
	}
}
let domains = counts.keys
for domain in domains.sort({$0 < $1}) {
	print(domain)
}