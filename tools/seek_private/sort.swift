import Foundation

let offset = 2

let text = try! String(contentsOfFile: "full.txt")
let lines = text.componentsSeparatedByString("\n")

for (index, line) in lines.enumerate() {
	if index % 3 == offset {
		print(line)
	}
}