import Foundation
let input = try! String(contentsOfFile:"/Users/andris/Desktop/raw_tracking.txt")
let fixDomains = ["b.scorecardresearch.com", "cdn.optimizely.com", "connect.facebook.net", "edge.quantserve.com", "js-agent.newrelic.com", "nexus.ensighten.com", "script.crazyegg.com", "ssl.google-analytics.com", "stats.wp.com", "www.google-analytics.com", "www.googletagservices.com", "static.chartbeat.com", "i.kissmetrics.com", "scripts.kissmetrics.com"]
let lines = input.componentsSeparatedByString("\n")
let noMatchPatterns = ["(?<!:|:/)/", "\\*", "(\\$|,)domain=[^~]", "(\\$|,)~third-party", "(\\$|,)object-subrequest", "(\\$|,)~script", "(\\$|,)image", "(\\$|,)subdocument", "^@"]
let domainPattern = "(?<=\\|\\||\\^|://)([\\w-]+\\.)+[\\w]+(?=\\^|:|/)"
let domainRX = try! NSRegularExpression(pattern: domainPattern, options: NSRegularExpressionOptions())
let noMatchRXs = noMatchPatterns.map {try! NSRegularExpression(pattern: $0, options: NSRegularExpressionOptions())}
let rules = lines.filter { s -> Bool in
	let nss = s as NSString
	let range = NSRange(location:0, length: nss.length)
	if domainRX.numberOfMatchesInString(s, options: NSMatchingOptions(), range: range) == 0 {
		return false
	}
	for rx in noMatchRXs {
		if rx.numberOfMatchesInString(s, options: NSMatchingOptions(), range: range) > 0 {
			return false
		}
	}
	return true
}
let filterdDomains = rules.map {s -> String in
	let nss = s as NSString
	let range = NSRange(location:0, length: nss.length)
	let match = domainRX.firstMatchInString(s, options: NSMatchingOptions(), range: range)!
	return nss.substringWithRange(match.range)
}
var counts = [String: Int]()
for domain in filterdDomains + fixDomains {
	if let count = counts[domain] {
		counts[domain] = count + 1
//		print(domain)
	} else {
		counts[domain] = 1
	}
}
let domains = counts.keys.sort {$0 < $1}
for domain in domains {
	print(domain)
}
